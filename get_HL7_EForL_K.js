const cron = require('node-cron');
const hl7 = require('simple-hl7');
const axios = require('axios')
const config = require('./config/init_config')
const PORT = config.port
const IP = config.ip
const URL = config.url

const client = hl7.Server.createTcpClient(IP, PORT);
console.log("agent start on ", IP, ':', PORT)

//create a message
const msg = new hl7.Message(
    "MSH|^~\&|||||||ACK^R01^ACK||P|2.4||||||ASCII||ASCIIMSA|AA|"
);

const setDatatoDB = async (room, parameter, valuevital_sign) => {
    let BaseRoomID = '/' + room + '/' + parameter
    URL_SEND = URL + 'api/NagativePressureRoom/vitalsigns' + BaseRoomID
    try {
        const response = await axios.put(URL_SEND, {
            value: valuevital_sign
        },
            {
                headers: {
                    "Content-Type": "application/json",
                    Authorization: 'FLD1t34rmIeeUuTBmLbDQAz7RZ03eln2ssuVZ8QuDOmQunwflZcaQbhvqhCKJCHC4mD892K5qruRQIN0d3WBiuFKUBp9Sx9cqPCyMDzBo68npUonQqt9tlvvZb6x0z520GZYeOt0dA4Obsy9nMsK0GMdxzbQjvNXaYk4hWjXAC4uIdZdhmEaqZTZl87lpyGGLaJk8ib3Y2FRGwrh1hApSVoWcyTcLOD2cxtlxPrZB5rP44VWjl80Cn614ez9IO9PKhcWpzdU3dlblu39v2aoevtV0mCuE2CBjXmPNA2RwlHgrx41oiOhD2eStZXafErwvi2rUC2TRUfM4PaIwc3sZovS4Xi8k6vtJ00QvLzC4uRLRM6awR0yBA12Dlzgtf71C1MXwwMYnt3WNN1AXH79ZCPxzlmRJyY2OJ33BLbGNdZZ9IRURE0X6QhfS1rOT54xaYkj3gXBe8CMTnh1Sg1L0qfYDL4SyKZb0gBOsEdiSzxH43Y7eYOY'
                },
            }
        )
        console.log(BaseRoomID, '', response.data.code, ": ", response.data.parameter, " ", response.data.message)
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
}


console.log('******sending message*****')
// console.log("msg =>",msg)

cron.schedule('*/1 * * * *', function () {
    // parameters 
    let HR = ''
    let O2SAT = ''
    let RR = ''
    let BP = ''
    let BPS = ''
    let MAP = ''
    let CVP = ''
    let ETCO2 = ''
    let TEMP = ''
    let ROOM = ''

    client.send(msg, (err, ack) => {

        if (ack === undefined) {
            console.log("Cannot get HL7")
        } else {
            message = ack.log()
            hl7_message = message.split("\n")
            console.log(hl7_message)

            hl7_message.forEach((index) => {
                // console.log('check =====>',index.split('|')[0]);

                ParameterName_tmp = ''
                if (index.split('|')[0] === 'MSH') {
                    console.log("-----------------------------------------------")
                    console.log("HEADING HL7")
                }
                else if (index.split('|')[0] === 'PID') {
                    console.log("-----------------------------------------------")
                    console.log("PATIENT")
                }
                if (index.split('|')[0] === 'PV1') {
                    // console.log("-----------------------------------------------")
                    // console.log("BED:", index.split('|')[3].split('^')[2].replace("&0&0",""))
                    console.log("-----------------------------------------------")
                    ROOM = index.split('|')[3].split('^')[2]
                    console.log("ROOM|", ROOM, "|")
                }
                else if (ROOM === 'BED-001') {
                    // console.log(index.split('|'))
                    // console.log("Date/Time =>", getDateTime())
                    if (index.split('|')[0] === 'OBX') {
                        OBX = index.split('|')
                        OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                        OBX5_VALUE_PARAMETER = OBX[5]
                        OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                        if (index.split('|')[2] === "NM") {
                            if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                                HR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13766001', 'HR', HR)

                            }
                            else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                                O2SAT = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13766001', 'O2SAT', O2SAT)

                            }

                            else if (OBX3_NAME_PARAMETER === 'VITAL RESP') {
                                RR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13766001', 'RR', RR)

                            }
                            else if (OBX3_NAME_PARAMETER === 'NIBP SYS') {
                                BPS = OBX5_VALUE_PARAMETER

                            }
                            else if (OBX3_NAME_PARAMETER === 'NIBP DIAS') {
                                BP = BPS + '/' + OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13766001', 'BP', BP)

                            }
                            else if (OBX3_NAME_PARAMETER === 'NIBP MEAN') {
                                MAP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13766001', 'MAP', MAP)

                            }
                            else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN') {
                                TEMP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13766001', 'TEMP', TEMP)

                            }
                            else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)') {
                                CVP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13766001', 'CVP', CVP)

                            }
                            else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2') {
                                ETCO2 = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13766001', 'ETCO2', ETCO2)

                            }
                        }
                    }
                    else {
                        // ถ้าไม่มี NM
                        // console.log('false');
                    }
                }
            })
        }
    });
})

///////////////////CLIENT/////////////////////
