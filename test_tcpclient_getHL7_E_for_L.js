const cron = require('node-cron');
const hl7 = require('simple-hl7');
const net = require('net')
const config = require('./config/init_config')
const PORT = config.port
const IP = config.ip
const IPSERVER = config.ipserever
const PORTSERVER = config.portserver
const URL = config.url


let client_tcp = new net.Socket()
const client = hl7.Server.createTcpClient(IP, PORT);
// const client_to_server = hl7.Server.createTcpClient(IPSERVER, PORTSERVER)
console.log("agent start on ", IP, ':', PORT)

//create a message
const msg = new hl7.Message(
    "EPIC",
    "EPICADT",
    "SMS",
    "199912271408",
    "CHARRIS",
    ["ADT", "A04"], //This field has 2 components
    "1817457",
    "D",
    "2.5"
);


console.log('******sending message*****')
// console.log("msg =>",msg)

let tmp = []

cron.schedule('*/5 * * * * *', function () {

    client.send(msg, (err, ack) => {
        
        if(ack === undefined){

            console.log("Cannot get HL7")

        }else{

            message = ack.log()
            // hl7_message = message.split("\n")
            // console.log(hl7_message)
            
            client_tcp.connect(PORTSERVER, IPSERVER, () => {
                // client_tcp.write('Hello World')
                
                tmp.push(message)
                console.log('tmp ===> ' + tmp)
                client_tcp.write(JSON.stringify(tmp));
                client_tcp.destroy()
            
            })
            client_tcp.on('data', (data) => {
                console.log('Incoming data from server : ', data)
            })
            console.log('Connecting to  ', IPSERVER, ':', PORTSERVER)
            
            
            // console.log("Can get HL7 \n", message.toString())
            
            }



    

    });

})

///////////////////CLIENT/////////////////////


