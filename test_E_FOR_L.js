const cron = require('node-cron');
const hl7 = require('simple-hl7');
const axios = require('axios')
const config = require('./config/init_config')
const PORT = config.port
const IP = config.ip
const IPSERVER = config.ipserever
const PORTSERVER = config.portserver
const URL = config.url


const client = hl7.Server.createTcpClient(IP, PORT);
// const client_to_server = hl7.Server.createTcpClient(IPSERVER, PORTSERVER)
console.log("agent start on ", IP, ':', PORT)

//create a message
const msg = new hl7.Message(
    "MSH|^~\&|||||||ACK^R01^ACK||P|2.4||||||ASCII||ASCIIMSA|AA|"
);



const setDatatoDB = async (room,parameter,valuevital_sign) => {
    let BaseRoomID = '/'+room+'/'+parameter
    URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
    try {
        const response = await axios.put(URL_SEND, {
            value: valuevital_sign
        },
            {
                headers: {
                    "Content-Type": "application/json",
                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                },
            }
        )
        console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message)
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
}

console.log('******sending message*****')
// console.log("msg =>",msg)



cron.schedule('*/5 * * * * *', function () {
    // parameters 
    let HR = ''
    let O2SAT = ''
    let RR = ''
    let BP = ''
    let BPS = ''
    let MAP = ''
    let CVP = ''
    let ETCO2 = ''
    let TEMP = ''
    let ROOM = ''

    client.send(msg, (err, ack) => {
        
        if(ack === undefined){

            console.log("Cannot get HL7")

        }else{

            message = ack.log()
            hl7_message = message.split("\n")
            console.log(hl7_message)
            hl7_message.forEach((index) => {
                
                ParameterName_tmp = ''
                if (index.split('|')[0] === 'MSH') {
                    console.log("-----------------------------------------------")
                    console.log("HEADING HL7")
                }
                else if (index.split('|')[0] === 'PID') {
                    console.log("-----------------------------------------------")
                    console.log("PATIENT")
                }
                if (index.split('|')[0] === 'PV1') {
                    // console.log("-----------------------------------------------")
                    // console.log("BED:", index.split('|')[3].split('^')[2].replace("&0&0",""))
                    console.log("-----------------------------------------------")
                    ROOM = index.split('|')[3].split('^')[2]
                    console.log("ROOM|",ROOM,"|")
                }
                else if (ROOM === 'ER-01') {
                    // console.log(index.split('|'))
                    // console.log("Date/Time =>", getDateTime())
                    if (index.split('|')[0] === 'OBX') {
                        OBX = index.split('|')
                        OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                        OBX5_VALUE_PARAMETER = OBX[5]
                        OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                        if (index.split('|')[2] === "NM" ){
                            if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                                HR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780009','HR', HR)
        
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                                O2SAT = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780009','O2SAT', O2SAT)
                                
                            } 
                            
                            else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                                RR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780009','RR', RR)
        
                            } 
                            else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                                BPS = OBX5_VALUE_PARAMETER
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                                BP = BPS +'/'+OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780009','BP', BP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                                MAP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780009','MAP', MAP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                                TEMP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780009','TEMP', TEMP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                                CVP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780009','CVP', CVP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                                ETCO2 = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780009','ETCO2', ETCO2)
                                
                            } 
                            
                        }
                    
                    }
                    else {
                        // ถ้าไม่มี NM 
                        
                    }
                    
                }
                else if (ROOM === 'ER-02') {
                    // console.log(index.split('|'))
                    // console.log("Date/Time =>", getDateTime())
                    if(index.split('|')[0] === 'OBX'){
                        OBX = index.split('|')
                        OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                        OBX5_VALUE_PARAMETER = OBX[5]
                        OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                        if (index.split('|')[2] === "NM" ){
                            if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                                HR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT137800010','HR', HR)
        
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                                O2SAT = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT137800010','O2SAT', O2SAT)
                                
                            } 
                            
                            else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                                RR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT137800010','RR', RR)
        
                            } 
                            else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                                BPS = OBX5_VALUE_PARAMETER
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                                BP = BPS +'/'+OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT137800010','BP', BP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                                MAP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT137800010','MAP', MAP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                                TEMP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT137800010','TEMP', TEMP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                                CVP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT137800010','CVP', CVP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                                ETCO2 = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT137800010','ETCO2', ETCO2)
                                
                            } 
                            
                        }
                    }
                    else {
                        // ถ้าไม่มี NM 
                        
                    }
                    
                }
                else if (ROOM === 'ER-03') {
                    // console.log(index.split('|'))
                    // console.log("Date/Time =>", getDateTime())
                    if (index.split('|')[0] === 'OBX') {
                        OBX = index.split('|')
                        OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                        OBX5_VALUE_PARAMETER = OBX[5]
                        OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                        if (index.split('|')[2] === "NM" ){
                            if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                                HR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT137800011','HR', HR)
        
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                                O2SAT = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT137800011','O2SAT', O2SAT)
                                
                            } 
                            
                            else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                                RR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT137800011','RR', RR)
        
                            } 
                            else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                                BPS = OBX5_VALUE_PARAMETER
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                                BP = BPS +'/'+OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT137800011','BP', BP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                                MAP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT137800011','MAP', MAP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                                TEMP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT137800011','TEMP', TEMP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                                CVP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT137800011','CVP', CVP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                                ETCO2 = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT137800011','ETCO2', ETCO2)
                                
                            } 
                        
                    }
                    }
                    else {
                        // ถ้าไม่มี NM 
                        
                    }
                    
                }
                else if (ROOM === 'ER-04') {
                    // console.log(index.split('|'))
                    // console.log("Date/Time =>", getDateTime())
                    if (index.split('|')[0] === 'OBX'){

                    }
                    OBX = index.split('|')
                    OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                    OBX5_VALUE_PARAMETER = OBX[5]
                    OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                    if (index.split('|')[2] === "NM" ){
                        if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                            HR = OBX5_VALUE_PARAMETER
                            setDatatoDB('IOT13780005','HR', HR)
    
                        } 
                        else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                            O2SAT = OBX5_VALUE_PARAMETER
                            setDatatoDB('IOT137800012','O2SAT', O2SAT)
                            
                        } 
                        
                        else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                            RR = OBX5_VALUE_PARAMETER
                            setDatatoDB('IOT137800012','RR', RR)
    
                        } 
                        else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                            BPS = OBX5_VALUE_PARAMETER
                            
                        }
                        else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                            BP = BPS +'/'+OBX5_VALUE_PARAMETER
                            setDatatoDB('IOT137800012','BP', BP)
                            
                        } 
                        else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                            MAP = OBX5_VALUE_PARAMETER
                            setDatatoDB('IOT137800012','MAP', MAP)
                            
                        } 
                        else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                            TEMP = OBX5_VALUE_PARAMETER
                            setDatatoDB('IOT137800012','TEMP', TEMP)
                            
                        } 
                        else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                            CVP = OBX5_VALUE_PARAMETER
                            setDatatoDB('IOT137800012','CVP', CVP)
                            
                        } 
                        else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                            ETCO2 = OBX5_VALUE_PARAMETER
                            setDatatoDB('IOT137800012','ETCO2', ETCO2)
                            
                        }
                        
                    }
                    else {
                        // ถ้าไม่มี NM 
                        
                    }
                    
                }
                else if (ROOM === 'Room104') {
                    // console.log(index.split('|'))
                    // console.log("Date/Time =>", getDateTime())
                    if (index.split('|')[0] === 'OBX'){
                        OBX = index.split('|')
                        OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                        OBX5_VALUE_PARAMETER = OBX[5]
                        OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                        if (index.split('|')[2] === "NM" ){
                            if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                                HR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780001','HR', HR)
        
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                                O2SAT = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780001','O2SAT', O2SAT)
                                
                            } 
                            
                            else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                                RR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780001','RR', RR)
        
                            } 
                            else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                                BPS = OBX5_VALUE_PARAMETER
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                                BP = BPS +'/'+OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780001','BP', BP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                                MAP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780001','MAP', MAP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                                TEMP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780001','TEMP', TEMP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                                CVP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780001','CVP', CVP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                                ETCO2 = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780001','ETCO2', ETCO2)
                                
                            }
                        }
                    }
                    else {
                        // ถ้าไม่มี NM
                        
                    }
                    
                }
                else if (ROOM === 'Room106') {
                    // console.log(index.split('|'))
                    // console.log("Date/Time =>", getDateTime())
                    if (index.split('|')[0] === 'OBX'){
                        OBX = index.split('|')
                        OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                        OBX5_VALUE_PARAMETER = OBX[5]
                        OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                        if (index.split('|')[2] === "NM" ){
                            if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                                HR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780002','HR', HR)
        
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                                O2SAT = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780002','O2SAT', O2SAT)
                                
                            } 
                            
                            else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                                RR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780002','RR', RR)
        
                            } 
                            else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                                BPS = OBX5_VALUE_PARAMETER
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                                BP = BPS +'/'+OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780002','BP', BP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                                MAP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780002','MAP', MAP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                                TEMP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780002','TEMP', TEMP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                                CVP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780002','CVP', CVP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                                ETCO2 = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780002','ETCO2', ETCO2)
                                
                            }
                            
                        }
                    }
                    else {
                        // ถ้าไม่มี NM 
                    }
                    
                }
                else if (ROOM === 'Room108') {
                    // console.log(index.split('|'))
                    // console.log("Date/Time =>", getDateTime())
                    if (index.split('|')[0] === 'OBX'){
                        OBX = index.split('|')
                        OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                        OBX5_VALUE_PARAMETER = OBX[5]
                        OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                        if (index.split('|')[2] === "NM" ){
                            if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                                HR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780003','HR', HR)
        
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                                O2SAT = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780003','O2SAT', O2SAT)
                                
                            } 
                            
                            else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                                RR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780003','RR', RR)
        
                            } 
                            else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                                BPS = OBX5_VALUE_PARAMETER
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                                BP = BPS +'/'+OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780003','BP', BP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                                MAP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780003','MAP', MAP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                                TEMP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780003','TEMP', TEMP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                                CVP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780003','CVP', CVP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                                ETCO2 = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780003','ETCO2', ETCO2)
                                
                            }
                            
                        }
                    }
                    else {
                        // ถ้าไม่มี NM 
                        
                    }
                    
                }
                else if (ROOM === 'Room110') {
                    // console.log(index.split('|'))
                    // console.log("Date/Time =>", getDateTime())
                    if (index.split('|')[0] === 'OBX'){
                        OBX = index.split('|')
                        OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                        OBX5_VALUE_PARAMETER = OBX[5]
                        OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                        if (index.split('|')[2] === "NM" ){
                            if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                                HR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780004','HR', HR)
        
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                                O2SAT = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780004','O2SAT', O2SAT)
                                
                            } 
                            
                            else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                                RR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780004','RR', RR)
        
                            } 
                            else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                                BPS = OBX5_VALUE_PARAMETER
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                                BP = BPS +'/'+OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780004','BP', BP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                                MAP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780004','MAP', MAP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                                TEMP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780004','TEMP', TEMP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                                CVP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780004','CVP', CVP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                                ETCO2 = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780004','ETCO2', ETCO2)
                                
                            }
                            
                        }
                    }
                    
                    else {
                        // ถ้าไม่มี NM 
                        
                    }
                    
                }
                else if (ROOM === 'Room112') {
                    // console.log(index.split('|'))
                    // console.log("Date/Time =>", getDateTime())
                    if (index.split('|')[0] === 'OBX'){
                        OBX = index.split('|')
                        OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                        OBX5_VALUE_PARAMETER = OBX[5]
                        OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                        if (index.split('|')[2] === "NM" ){
                            if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                                HR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780005','HR', HR)
        
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                                O2SAT = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780005','O2SAT', O2SAT)
                                
                            } 
                            
                            else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                                RR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780005','RR', RR)
        
                            } 
                            else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                                BPS = OBX5_VALUE_PARAMETER
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                                BP = BPS +'/'+OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780005','BP', BP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                                MAP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780005','MAP', MAP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                                TEMP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780005','TEMP', TEMP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                                CVP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780005','CVP', CVP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                                ETCO2 = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780005','ETCO2', ETCO2)
                                
                            }
                            
                        }
                    }
                    
                    else {
                        // ถ้าไม่มี NM 
                        
                    }
                    
                }
                else if (ROOM === 'Room114') {
                    // console.log(index.split('|'))
                    // console.log("Date/Time =>", getDateTime())
                    if (index.split('|')[0] === 'OBX'){
                        OBX = index.split('|')
                        OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                        OBX5_VALUE_PARAMETER = OBX[5]
                        OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                        if (index.split('|')[2] === "NM" ){
                            if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                                HR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780006','HR', HR)
        
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                                O2SAT = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780006','O2SAT', O2SAT)
                                
                            } 
                            
                            else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                                RR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780006','RR', RR)
        
                            } 
                            else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                                BPS = OBX5_VALUE_PARAMETER
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                                BP = BPS +'/'+OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780006','BP', BP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                                MAP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780006','MAP', MAP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                                TEMP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780006','TEMP', TEMP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                                CVP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780006','CVP', CVP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                                ETCO2 = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780006','ETCO2', ETCO2)
                                
                            }
                            
                        }
                    }
                    
                    else {
                        // ถ้าไม่มี NM 
                        
                    }
                    
                }
                else if (ROOM === 'Room126') {
                    // console.log(index.split('|'))
                    // console.log("Date/Time =>", getDateTime())
                    if (index.split('|')[0] === 'OBX'){
                        OBX = index.split('|')
                        OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                        OBX5_VALUE_PARAMETER = OBX[5]
                        OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                        if (index.split('|')[2] === "NM" ){
                            if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                                HR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780007','HR', HR)
        
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                                O2SAT = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780007','O2SAT', O2SAT)
                                
                            } 
                            
                            else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                                RR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780007','RR', RR)
        
                            } 
                            else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                                BPS = OBX5_VALUE_PARAMETER
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                                BP = BPS +'/'+OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780007','BP', BP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                                MAP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780007','MAP', MAP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                                TEMP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780007','TEMP', TEMP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                                CVP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780007','CVP', CVP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                                ETCO2 = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780007','ETCO2', ETCO2)
                                
                            }
                            
                        }
                    }
                    
                    else {
                        // ถ้าไม่มี NM 
                        
                    }
                    
                }
                else if (ROOM === 'Room128') {
                    // console.log(index.split('|'))
                    // console.log("Date/Time =>", getDateTime())
                    if (index.split('|')[0] === 'OBX'){
                        OBX = index.split('|')
                        OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                        OBX5_VALUE_PARAMETER = OBX[5]
                        OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                        if (index.split('|')[2] === "NM" ){
                            if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                                HR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780008','HR', HR)
        
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                                O2SAT = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780008','O2SAT', O2SAT)
                                
                            } 
                            
                            else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                                RR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780008','RR', RR)
        
                            } 
                            else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                                BPS = OBX5_VALUE_PARAMETER
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                                BP = BPS +'/'+OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780008','BP', BP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                                MAP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780008','MAP', MAP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                                TEMP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780008','TEMP', TEMP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                                CVP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780008','CVP', CVP)
                                
                            } 
                            else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                                ETCO2 = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT13780008','ETCO2', ETCO2)
                                
                            }
                            
                        }
                    }
                    
                    else {
                        // ถ้าไม่มี NM 
                        
                    }
                    
                }
                
    
            })
            
            
            
            // console.log("Can get HL7 \n", message.toString())
            
            }



    

    });

})

///////////////////CLIENT/////////////////////


