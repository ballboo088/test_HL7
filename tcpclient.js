let net = require('net');
const PORT = 1337;
const HOST = 'localhost';
let client = new net.Socket()

client.connect(PORT, HOST, () => {
    client.write('Hello World')
    client.destroy()

})
client.on('data', (data) => {
    console.log('Incoming data from server : ', data)
})
console.log('Connecting to  ', HOST, ':', PORT)