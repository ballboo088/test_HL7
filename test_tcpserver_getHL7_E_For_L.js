let axios = require('axios')
let net = require('net');
const HOST = '0.0.0.0' ;
const PORT = 1337;
const URL = 'http://172.29.22.61/'

let HR = ''
let O2SAT = ''
let RR = ''
let BP = ''
let BPS = ''
let MAP = ''
let CVP = ''
let ETCO2 = ''
let TEMP = ''
let ROOM = ''


let server = net.createServer((sock) => {
    console.log('Incoming client from :'+ sock.remoteAddress + ':' + sock.remotePort )

    sock.on('data', (data)=> {
        // console.log('Incoming data: '+ data)
        datas = JSON.parse(data)
        cl
        // sock.write(datas.code)
        hl7_message = datas.split("\n")
        console.log(hl7_message)
        hl7_message.forEach((index) => {
            ParameterName_tmp = ''
            if (index.split('|')[0] === 'MSH') {
                console.log("-----------------------------------------------")
                console.log("HEADING HL7")
            }
            else if (index.split('|')[0] === 'PID') {
                console.log("-----------------------------------------------")
                console.log("PATIENT")
            }
            if (index.split('|')[0] === 'PV1') {
                // console.log("-----------------------------------------------")
                // console.log("BED:", index.split('|')[3].split('^')[2].replace("&0&0",""))
                console.log("-----------------------------------------------")
                ROOM = index.split('|')[3].split('^')[2]
                console.log("ROOM|",ROOM,"|")
            }
            else if (index.split('|')[0] === 'OBX' && ROOM === 'ER-01') {
                // console.log(index.split('|'))
                // console.log("Date/Time =>", getDateTime())
                OBX = index.split('|')
                OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                OBX5_VALUE_PARAMETER = OBX[5]
                OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                if (index.split('|')[2] === "NM" ){
                    if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                        HR = OBX5_VALUE_PARAMETER
                        // console.log("HR =>", HR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780009'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                        O2SAT = OBX5_VALUE_PARAMETER
                        // console.log("O2SAT =>", O2SAT, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780009'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    
                    } 
                    
                    else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                        RR = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780009'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                        BPS = OBX5_VALUE_PARAMETER
                        
                    }
                    else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                        BP = BPS +'/'+OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780009'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                        MAP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780009'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                        TEMP = OBX5_VALUE_PARAMETER
                        let BaseRoomID = '/IOT13780009'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: TEMP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                        CVP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780009'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                        ETCO2 = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780009'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if(HR === ''){
                        HR = '-'
                        let BaseRoomID = '/IOT13780009'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    else if(O2SAT === ''){
                        O2SAT = '-'
                        let BaseRoomID = '/IOT13780009'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    else if(RR === ''){
                        RR = '-'
                        let BaseRoomID = '/IOT13780009'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    else if(BPS === ''){
                        BPS = '-'
                    }
                    else if(BP === ''){
                        BP = BPS +'/'+ '-'
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780009'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    }
                    else if(MAP === ''){
                        MAP = '-'
                        let BaseRoomID = '/IOT13780009'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    else if(TEMP === ''){
                        TEMP = '-'
                        let BaseRoomID = '/IOT13780009'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: TEMP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    else if(CVP === ''){
                        CVP = '-'
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780009'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    else if(ETCO2 === ''){
                        ETCO2 = '-'
                        let BaseRoomID = '/IOT13780009'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    
                }
                else {
                    // ถ้าไม่มี NM 
                    
                }
                
            }
            else if (index.split('|')[0] === 'OBX' && ROOM === 'ER-02') {
                // console.log(index.split('|'))
                // console.log("Date/Time =>", getDateTime())
                OBX = index.split('|')
                OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                OBX5_VALUE_PARAMETER = OBX[5]
                OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                if (index.split('|')[2] === "NM" ){
                    if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                        HR = OBX5_VALUE_PARAMETER
                        // console.log("HR =>", HR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800010'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                        O2SAT = OBX5_VALUE_PARAMETER
                        // console.log("O2SAT =>", O2SAT, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800010'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    
                    } 
                    
                    else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                        RR = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800010'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                        BPS = OBX5_VALUE_PARAMETER
                        
                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                        BP = BPS + '/'+ OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800010'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                        MAP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800010'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                        TEMP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800010'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                        CVP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800010'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                        ETCO2 = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800010'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    
                }
                else {
                    // ถ้าไม่มี NM 
                    if(HR === ''){
                        HR = '-'
                        let BaseRoomID = '/IOT137800010'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    if(O2SAT === ''){
                        O2SAT = '-'
                        let BaseRoomID = '/IOT137800010'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(RR === ''){
                        RR = '-'
                        let BaseRoomID = '/IOT137800010'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(BPS === ''){
                        BPS = '-'
                    }
                    if(BP === ''){
                        BP = BPS +'/'+ '-'
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800010'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    }
                    if(MAP === ''){
                        MAP = '-'
                        let BaseRoomID = '/IOT137800010'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    if(TEMP === ''){
                        TEMP = '-'
                        let BaseRoomID = '/IOT137800010'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: TEMP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(CVP === ''){
                        CVP = '-'
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800010'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(ETCO2 === ''){
                        ETCO2 = '-'
                        let BaseRoomID = '/IOT137800010'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                }
                
            }
            else if (index.split('|')[0] === 'OBX' && ROOM === 'ER-03') {
                // console.log(index.split('|'))
                // console.log("Date/Time =>", getDateTime())
                OBX = index.split('|')
                OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                OBX5_VALUE_PARAMETER = OBX[5]
                OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                if (index.split('|')[2] === "NM" ){
                    if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                        HR = OBX5_VALUE_PARAMETER
                        // console.log("HR =>", HR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800011'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                        O2SAT = OBX5_VALUE_PARAMETER
                        // console.log("O2SAT =>", O2SAT, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800011'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    
                    } 
                    
                    else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                        RR = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800011'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                        BPS = OBX5_VALUE_PARAMETER
                        
                    }
                    else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                        BP = BPS +'/'+OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800011'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                        MAP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800011'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                        TEMP = OBX5_VALUE_PARAMETER
                        let BaseRoomID = '/IOT137800011'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: TEMP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                        CVP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800011'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                        ETCO2 = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800011'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    
                }
                else {
                    // ถ้าไม่มี NM 
                    if(HR === ''){
                        HR = '-'
                        let BaseRoomID = '/IOT137800011'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    if(O2SAT === ''){
                        O2SAT = '-'
                        let BaseRoomID = '/IOT137800011'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(RR === ''){
                        RR = '-'
                        let BaseRoomID = '/IOT137800011'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(BPS === ''){
                        BPS = '-'
                    }
                    if(BP === ''){
                        BP = BPS +'/'+ '-'
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800011'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    }
                    if(MAP === ''){
                        MAP = '-'
                        let BaseRoomID = '/IOT137800011'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    if(TEMP === ''){
                        TEMP = '-'
                        let BaseRoomID = '/IOT137800011'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: TEMP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(CVP === ''){
                        CVP = '-'
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800011'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(ETCO2 === ''){
                        ETCO2 = '-'
                        let BaseRoomID = '/IOT137800011'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                }
                
            }
            else if (index.split('|')[0] === 'OBX' && ROOM === 'ER-04') {
                // console.log(index.split('|'))
                // console.log("Date/Time =>", getDateTime())
                OBX = index.split('|')
                OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                OBX5_VALUE_PARAMETER = OBX[5]
                OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                if (index.split('|')[2] === "NM" ){
                    if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                        HR = OBX5_VALUE_PARAMETER
                        // console.log("HR =>", HR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800012'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                        O2SAT = OBX5_VALUE_PARAMETER
                        // console.log("O2SAT =>", O2SAT, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800012'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    
                    } 
                    
                    else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                        RR = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800012'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                        BPS = OBX5_VALUE_PARAMETER
                    }
                    else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                        BP = BPS +'/'+OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800012'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                        MAP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800012'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                        TEMP = OBX5_VALUE_PARAMETER
                        let BaseRoomID = '/IOT137800012'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: TEMP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                        CVP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800012'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                        ETCO2 = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT137800012'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    
                }
                else {
                    // ถ้าไม่มี NM 
                    if(HR === ''){
                        HR = '-'
                        let BaseRoomID = '/IOT137800012'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    if(O2SAT === ''){
                        O2SAT = '-'
                        let BaseRoomID = '/IOT137800012'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(RR === ''){
                        RR = '-'
                        let BaseRoomID = '/IOT137800012'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(BPS === ''){
                        BPS = '-'
                    }
                    if(BP === ''){
                        BP = BPS +'/'+ '-'
                        let BaseRoomID = '/IOT137800012'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    }
                    if(MAP === ''){
                        MAP = '-'
                        let BaseRoomID = '/IOT137800012'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    if(TEMP === ''){
                        TEMP = '-'
                        let BaseRoomID = '/IOT137800012'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: TEMP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(CVP === ''){
                        CVP = '-'
                        let BaseRoomID = '/IOT137800012'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(ETCO2 === ''){
                        ETCO2 = '-'
                        let BaseRoomID = '/IOT137800012'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                }
                
            }
            else if (index.split('|')[0] === 'OBX' && ROOM === 'Room104') {
                // console.log(index.split('|'))
                // console.log("Date/Time =>", getDateTime())
                OBX = index.split('|')
                OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                OBX5_VALUE_PARAMETER = OBX[5]
                OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                if (index.split('|')[2] === "NM" ){
                    if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                        HR = OBX5_VALUE_PARAMETER
                        // console.log("HR =>", HR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780001'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                        O2SAT = OBX5_VALUE_PARAMETER
                        // console.log("O2SAT =>", O2SAT, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780001'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    
                    } 
                    
                    else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                        RR = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780001'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                        BPS = OBX5_VALUE_PARAMETER
                        
                    }
                    else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                        BP = BPS +'/'+OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780001'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                        MAP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780001'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                        TEMP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780001'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                        CVP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780001'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                        ETCO2 = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780001'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    
                }
                else {
                    // ถ้าไม่มี NM 
                    if(HR === ''){
                        HR = '-'
                        let BaseRoomID = '/IOT13780001'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    if(O2SAT === ''){
                        O2SAT = '-'
                        let BaseRoomID = '/IOT13780001'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(RR === ''){
                        RR = '-'
                        let BaseRoomID = '/IOT13780001'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(BPS === ''){
                        BPS = '-'
                    }
                    if(BP === ''){
                        BP = BPS +'/'+ '-'
                        let BaseRoomID = '/IOT13780001'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    }
                    if(MAP === ''){
                        MAP = '-'
                        let BaseRoomID = '/IOT13780001'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    if(TEMP === ''){
                        TEMP = '-'
                        let BaseRoomID = '/IOT13780001'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: TEMP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(CVP === ''){
                        CVP = '-'
                        let BaseRoomID = '/IOT13780001'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(ETCO2 === ''){
                        ETCO2 = '-'
                        let BaseRoomID = '/IOT13780001'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    
                }
                
            }
            else if (index.split('|')[0] === 'OBX' && ROOM === 'Room106') {
                // console.log(index.split('|'))
                // console.log("Date/Time =>", getDateTime())
                OBX = index.split('|')
                OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                OBX5_VALUE_PARAMETER = OBX[5]
                OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                if (index.split('|')[2] === "NM" ){
                    if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                        HR = OBX5_VALUE_PARAMETER
                        // console.log("HR =>", HR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780002'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                        O2SAT = OBX5_VALUE_PARAMETER
                        // console.log("O2SAT =>", O2SAT, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780002'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    
                    }                   
                    else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                        RR = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780002'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                        BPS = OBX5_VALUE_PARAMETER
                        
                    }
                    else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                        BP = BPS +'/'+OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780002'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                        MAP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780002'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                        TEMP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780002'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                        CVP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780002'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                        ETCO2 = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780002'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    
                }
                else {
                    // ถ้าไม่มี NM 
                    if(HR === ''){
                        HR = '-'
                        let BaseRoomID = '/IOT13780002'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    if(O2SAT === ''){
                        O2SAT = '-'
                        let BaseRoomID = '/IOT13780002'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(RR === ''){
                        RR = '-'
                        let BaseRoomID = '/IOT13780002'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(BPS === ''){
                        BPS = '-'
                    }
                    if(BP === ''){
                        BP = BPS +'/'+ '-'
                        let BaseRoomID = '/IOT13780002'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    }
                    if(MAP === ''){
                        MAP = '-'
                        let BaseRoomID = '/IOT13780002'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    if(TEMP === ''){
                        TEMP = '-'
                        let BaseRoomID = '/IOT13780002'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: TEMP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(CVP === ''){
                        CVP = '-'
                        let BaseRoomID = '/IOT13780002'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(ETCO2 === ''){
                        ETCO2 = '-'
                        let BaseRoomID = '/IOT13780002'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                }
                
            }
            else if (index.split('|')[0] === 'OBX' && ROOM === 'Room108') {
                // console.log(index.split('|'))
                // console.log("Date/Time =>", getDateTime())
                OBX = index.split('|')
                OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                OBX5_VALUE_PARAMETER = OBX[5]
                OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                if (index.split('|')[2] === "NM" ){
                    if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                        HR = OBX5_VALUE_PARAMETER
                        // console.log("HR =>", HR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780003'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                        O2SAT = OBX5_VALUE_PARAMETER
                        // console.log("O2SAT =>", O2SAT, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780003'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    
                    } 
                    
                    else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                        RR = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780003'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                        BPS = OBX5_VALUE_PARAMETER
                        
                    }
                    else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                        BP = BPS +'/'+OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780003'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                        MAP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780003'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                        TEMP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780003'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                        CVP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780003'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                        ETCO2 = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780003'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    
                }
                else {
                    // ถ้าไม่มี NM 
                    if(HR === ''){
                        HR = '-'
                        let BaseRoomID = '/IOT13780003'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    if(O2SAT === ''){
                        O2SAT = '-'
                        let BaseRoomID = '/IOT13780003'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(RR === ''){
                        RR = '-'
                        let BaseRoomID = '/IOT13780003'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(BPS === ''){
                        BPS = '-'
                    }
                    if(BP === ''){
                        BP = BPS +'/'+ '-'
                        let BaseRoomID = '/IOT13780003'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    }
                    if(MAP === ''){
                        MAP = '-'
                        let BaseRoomID = '/IOT13780003'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    if(TEMP === ''){
                        TEMP = '-'
                        let BaseRoomID = '/IOT13780003'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: TEMP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(CVP === ''){
                        CVP = '-'
                        let BaseRoomID = '/IOT13780003'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(ETCO2 === ''){
                        ETCO2 = '-'
                        let BaseRoomID = '/IOT13780003'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                }
                
            }
            else if (index.split('|')[0] === 'OBX' && ROOM === 'Room110') {
                // console.log(index.split('|'))
                // console.log("Date/Time =>", getDateTime())
                OBX = index.split('|')
                OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                OBX5_VALUE_PARAMETER = OBX[5]
                OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                if (index.split('|')[2] === "NM" ){
                    if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                        HR = OBX5_VALUE_PARAMETER
                        // console.log("HR =>", HR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780004'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                        O2SAT = OBX5_VALUE_PARAMETER
                        // console.log("O2SAT =>", O2SAT, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780004'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    
                    } 
                    
                    else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                        RR = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780004'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                        BPS = OBX5_VALUE_PARAMETER
                        
                    }
                    else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                        BP = BPS +'/'+OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780004'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                        MAP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780004'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                        TEMP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780004'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                        CVP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780004'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                        ETCO2 = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780004'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    
                }
                else {
                    // ถ้าไม่มี NM 
                    if(HR === ''){
                        HR = '-'
                        let BaseRoomID = '/IOT13780004'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    if(O2SAT === ''){
                        O2SAT = '-'
                        let BaseRoomID = '/IOT13780004'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(RR === ''){
                        RR = '-'
                        let BaseRoomID = '/IOT13780004'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(BPS === ''){
                        BPS = '-'
                    }
                    if(BP === ''){
                        BP = BPS +'/'+ '-'
                        let BaseRoomID = '/IOT13780004'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    }
                    if(MAP === ''){
                        MAP = '-'
                        let BaseRoomID = '/IOT13780004'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    if(TEMP === ''){
                        TEMP = '-'
                        let BaseRoomID = '/IOT13780004'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: TEMP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(CVP === ''){
                        CVP = '-'
                        let BaseRoomID = '/IOT13780004'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(ETCO2 === ''){
                        ETCO2 = '-'
                        let BaseRoomID = '/IOT13780004'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                }
                
            }
            else if (index.split('|')[0] === 'OBX' && ROOM === 'Room112') {
                // console.log(index.split('|'))
                // console.log("Date/Time =>", getDateTime())
                OBX = index.split('|')
                OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                OBX5_VALUE_PARAMETER = OBX[5]
                OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                if (index.split('|')[2] === "NM" ){
                    if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                        HR = OBX5_VALUE_PARAMETER
                        // console.log("HR =>", HR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780005'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                        O2SAT = OBX5_VALUE_PARAMETER
                        // console.log("O2SAT =>", O2SAT, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780005'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    
                    } 
                    
                    else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                        RR = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780005'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                        BPS = OBX5_VALUE_PARAMETER
                        
                    }
                    else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                        BP = BPS +'/'+OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780005'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                        MAP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780005'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                        TEMP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780005'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                        CVP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780005'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                        ETCO2 = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780005'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    
                }
                else {
                    // ถ้าไม่มี NM 
                    if(HR === ''){
                        HR = '-'
                        let BaseRoomID = '/IOT13780005'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    if(O2SAT === ''){
                        O2SAT = '-'
                        let BaseRoomID = '/IOT13780005'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(RR === ''){
                        RR = '-'
                        let BaseRoomID = '/IOT13780005'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(BPS === ''){
                        BPS = '-'
                    }
                    if(BP === ''){
                        BP = BPS +'/'+ '-'
                        let BaseRoomID = '/IOT13780005'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    }
                    if(MAP === ''){
                        MAP = '-'
                        let BaseRoomID = '/IOT13780005'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    if(TEMP === ''){
                        TEMP = '-'
                        let BaseRoomID = '/IOT13780005'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: TEMP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(CVP === ''){
                        CVP = '-'
                        let BaseRoomID = '/IOT13780005'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(ETCO2 === ''){
                        ETCO2 = '-'
                        let BaseRoomID = '/IOT13780005'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                }
                
            }
            else if (index.split('|')[0] === 'OBX' && ROOM === 'Room114') {
                // console.log(index.split('|'))
                // console.log("Date/Time =>", getDateTime())
                OBX = index.split('|')
                OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                OBX5_VALUE_PARAMETER = OBX[5]
                OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                if (index.split('|')[2] === "NM" ){
                    if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                        HR = OBX5_VALUE_PARAMETER
                        // console.log("HR =>", HR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780006'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                        O2SAT = OBX5_VALUE_PARAMETER
                        // console.log("O2SAT =>", O2SAT, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780006'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    
                    } 
                    
                    else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                        RR = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780006'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                        BPS = OBX5_VALUE_PARAMETER
                        
                    }
                    else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                        BP = BPS +'/'+OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780006'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                        MAP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780006'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                        TEMP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780006'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                        CVP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780006'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                        ETCO2 = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780006'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    
                }
                else {
                    // ถ้าไม่มี NM 
                    if(HR === ''){
                        HR = '-'
                        let BaseRoomID = '/IOT13780006'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    if(O2SAT === ''){
                        O2SAT = '-'
                        let BaseRoomID = '/IOT13780006'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(RR === ''){
                        RR = '-'
                        let BaseRoomID = '/IOT13780006'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(BPS === ''){
                        BPS = '-'
                    }
                    if(BP === ''){
                        BP = BPS +'/'+ '-'
                        let BaseRoomID = '/IOT13780006'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    }
                    if(MAP === ''){
                        MAP = '-'
                        let BaseRoomID = '/IOT13780006'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    if(TEMP === ''){
                        TEMP = '-'
                        let BaseRoomID = '/IOT13780006'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: TEMP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(CVP === ''){
                        CVP = '-'
                        let BaseRoomID = '/IOT13780006'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(ETCO2 === ''){
                        ETCO2 = '-'
                        let BaseRoomID = '/IOT13780006'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                }
                
            }
            else if (index.split('|')[0] === 'OBX' && ROOM === 'Room126') {
                // console.log(index.split('|'))
                // console.log("Date/Time =>", getDateTime())
                OBX = index.split('|')
                OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                OBX5_VALUE_PARAMETER = OBX[5]
                OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                if (index.split('|')[2] === "NM" ){
                    if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                        HR = OBX5_VALUE_PARAMETER
                        // console.log("HR =>", HR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780007'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                        O2SAT = OBX5_VALUE_PARAMETER
                        // console.log("O2SAT =>", O2SAT, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780007'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    
                    } 
                    
                    else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                        RR = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780007'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                        BPS = OBX5_VALUE_PARAMETER
                        
                    }
                    else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                        BP = BPS +'/'+OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780007'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                        MAP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780007'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                        TEMP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780007'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                        CVP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780007'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                        ETCO2 = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780007'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    
                }
                else {
                    // ถ้าไม่มี NM 
                    if(HR === ''){
                        HR = '-'
                        let BaseRoomID = '/IOT13780007'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    if(O2SAT === ''){
                        O2SAT = '-'
                        let BaseRoomID = '/IOT13780007'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(RR === ''){
                        RR = '-'
                        let BaseRoomID = '/IOT13780007'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(BPS === ''){
                        BPS = '-'
                    }
                    if(BP === ''){
                        BP = BPS +'/'+ '-'
                        let BaseRoomID = '/IOT13780007'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    }
                    if(MAP === ''){
                        MAP = '-'
                        let BaseRoomID = '/IOT13780007'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    if(TEMP === ''){
                        TEMP = '-'
                        let BaseRoomID = '/IOT13780007'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: TEMP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(CVP === ''){
                        CVP = '-'
                        let BaseRoomID = '/IOT13780007'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(ETCO2 === ''){
                        ETCO2 = '-'
                        let BaseRoomID = '/IOT13780007'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                }
                
            }
            else if (index.split('|')[0] === 'OBX' && ROOM === 'Room128') {
                // console.log(index.split('|'))
                // console.log("Date/Time =>", getDateTime())
                OBX = index.split('|')
                OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                OBX5_VALUE_PARAMETER = OBX[5]
                OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                if (index.split('|')[2] === "NM" ){
                    if (OBX3_NAME_PARAMETER === 'VITAL HR') {
                        HR = OBX5_VALUE_PARAMETER
                        // console.log("HR =>", HR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780008'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL SpO2') {
                        O2SAT = OBX5_VALUE_PARAMETER
                        // console.log("O2SAT =>", O2SAT, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780008'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    
                    } 
                    
                    else if (OBX3_NAME_PARAMETER === 'VITAL RESP'){
                        RR = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780008'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    }   
                    else if (OBX3_NAME_PARAMETER === 'NIBP SYS'){
                        BPS = OBX5_VALUE_PARAMETER
                        
                    }
                    else if (OBX3_NAME_PARAMETER === 'NIBP DIAS'){
                        BP = BPS +'/'+OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780008'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })


                    } 
                    else if (OBX3_NAME_PARAMETER === 'NIBP MEAN'){
                        MAP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780008'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    else if (OBX3_NAME_PARAMETER === 'VITAL TSKIN'){
                        TEMP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780008'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL CVP(M)'){
                        CVP = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780008'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    else if (OBX3_NAME_PARAMETER === 'VITAL EtCO2'){
                        ETCO2 = OBX5_VALUE_PARAMETER
                        // console.log("RR =>", RR, OBX6_UNIT_PARAMETER)
                        let BaseRoomID = '/IOT13780008'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    } 
                    
                }
                else {
                    // ถ้าไม่มี NM 
                    if(HR === ''){
                        HR = '-'
                        let BaseRoomID = '/IOT13780008'+'/HR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: HR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    if(O2SAT === ''){
                        O2SAT = '-'
                        let BaseRoomID = '/IOT13780008'+'/O2SAT'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: O2SAT
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(RR === ''){
                        RR = '-'
                        let BaseRoomID = '/IOT13780008'+'/RR'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: RR
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(BPS === ''){
                        BPS = '-'
                    }
                    if(BP === ''){
                        BP = BPS +'/'+ '-'
                        let BaseRoomID = '/IOT13780008'+'/BP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: BP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })

                    }
                    if(MAP === ''){
                        MAP = '-'
                        let BaseRoomID = '/IOT13780008'+'/MAP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: MAP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);
                        })
                    }
                    if(TEMP === ''){
                        TEMP = '-'
                        let BaseRoomID = '/IOT13780008'+'/TEMP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: TEMP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(CVP === ''){
                        CVP = '-'
                        let BaseRoomID = '/IOT13780008'+'/CVP'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: CVP
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                    if(ETCO2 === ''){
                        ETCO2 = '-'
                        let BaseRoomID = '/IOT13780008'+'/ETCO2'
                        URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
                        axios.put(URL_SEND, {
                            value: ETCO2
                        },
                            {
                                headers: {
                                    "Content-Type": "application/json",
                                    Authorization: 'fEMqUuQ5G3qWfe5LhzCmQKqDHtTYMgnp7GGLttXK2mBva4zgrURxM2WLxSzzNJ7SvsxkR6qKynGVzmtmGbMtJVCGkza5TRqDXfzJF2f8tWCXpXShhYezvApnzqJRSqWAVcnH4Dc6gNgyLreNNmZM5nBUXf4rQEq3Xy4zEpArGRS7d8nxnDfjQFaA3bWQJHVhFnhWnjQyLZdegGvytcBHjmDGbAMQtfCA593gwUhC4rhu65KW8bnsCgYcP9TSFK7R'
                                },
                            }
                        ).then((response) => {
                            console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message);

                        })
                    }
                }
                
            }


        })
    })
    sock.on('close', (data) => {
        console.log('client is closing connection')
    })
    sock.on('error', (error) => {
        console.log('Error : ', error)
    })
})
server.listen(PORT,HOST)
console.log('Server is running at : ', HOST, ':', PORT)