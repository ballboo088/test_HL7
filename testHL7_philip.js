const hl7 = require('simple-hl7')
const axios = require('axios')
const cron = require('node-cron');
const config = require('./config/init_config')

const PORT = config.port
const IP = config.ip
const URL = config.url

const app = hl7.tcp()
let i = 0 ; 
const TMP = {}

function getDateTime() {

    let date = new Date();

    let hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    let min = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    let sec = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    let year = date.getFullYear();

    let month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    let day = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;

}

console.log('******AGENT AND LISTENS ON PORT :' + IP + ':' + PORT + ' *****')
//HR, BP, O2SAT, MAP, RR, TEMP, ETCO2, ARR
let HR = ''
let O2SAT = ''
let RR = ''
let BP = ''
let ROOM = ''

console.log('******message HL7 received*****')

app.use((req, res, next) => {

    if (req) {
        let date = getDateTime() ;

        message_tmp = {} ; 
        message_tmp = req.msg.log() ; 
        message_strc = req.msg ; 
        // console.log("message =>", message_tmp)

        hl7_message = message_tmp.split("\n")
        console.log(hl7_message)
                
    }
    else {
        console.log("NO MESSAGE")
    }
})
app.use((err, req, res, next) => {
    
    console.log('******ERROR*****')
    console.log(err);
    
})

app.start(PORT);