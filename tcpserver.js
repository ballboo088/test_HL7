let net = require('net');
const HOST = '0.0.0.0' ;
const PORT = 1337
let server = net.createServer((sock) => {
    console.log('Incoming client from :'+ sock.remoteAddress + ':' + sock.remotePort )

    sock.on('data', (data)=> {
        console.log('Incoming data: '+ data)
    })
    sock.on('close', (data) => {
        console.log('client is closing connection')
    })
    sock.on('error', (error) => {
        console.log('Error : ', error)
    })
})
server.listen(PORT,HOST)
console.log('Server is running at : ', HOST, ':', PORT)