var axios = require('axios');
var mssql = require('mssql');
let cron = require('node-cron')
let Token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MDgwOTE1NzgsImp0aSI6IjVmZDk4N2JhNzMwM2EiLCJpc3MiOiJzdWFuZG9rX3BhY3NfaW90X2ludGVyZmFjZSIsIm5iZiI6MTYwODA5MTU3OCwiZXhwIjoxNzY1NzcxNTc4LCJkYXRhIjp7InBpZCI6IiIsImhuIjoiIiwibmFtZSI6IklPVCBQQUNTIiwic191aWQiOiIiLCJncm91cCI6IjAiLCJyb2xlIjoiMCIsImFwcCI6IklWbXRXVW1KVldsaFdiWGh6VFRGYWRHTkZTbXhTYlZKIn19.Yni8NiOKb2ddiI-onooO0TLjfc94ZJJnaGlgSLOCiH8';

const SqlserverConfig = {
    user: "develop",
    password: "develop",
    server: "172.29.22.61",
    database: "iotserver",
    port: 1433,
    options: {
        enableArithAbort: true,
        encrypt: false
    },
};

const config_MDR0 = {
    method: 'get',
    url: 'http://hosweb.med.cmu.ac.th/gateway/ward/MDR0/patients',
    headers: {
        'Authorization': 'Bearer ' + Token + ''
    }
};

const config_ER = {
    method: 'get',
    url: 'http://hosweb.med.cmu.ac.th/gateway/opd/er/patients',
    headers: {
        'Authorization': 'Bearer ' + Token + ''
    }
};


cron.schedule('*/2 * * * *', () => {
    mssql.connect(SqlserverConfig)
    .then((result) => {
        console.log('** connect sql server **')
        var sqlRequest = new mssql.Request();

        axios(config_MDR0)
            .then(function (response) {

                response.data.map((data) => {
                    let Pid = data.pid;
                    let RoomName = 'Room ' + data.bed_name + '';
                    let sqlQuery = 'exec [dbo].[updatePidBaseMonitor] @NumberRoomName ="' + RoomName + '", @NumberPid = "' + Pid + '"';

                    sqlRequest.query(sqlQuery, function (err, data) {
                        if (data) {
                            console.log('update => RoomName:', RoomName, 'Pid:', Pid);
                        } else {
                            console.log('not update');
                        }
                    });

                    // console.log(Pid);
                    // console.log(RoomName);
                    // console.log(sqlQuery);
                });

            })
            .catch(function (err) {
                console.log('error :', err);
            });

        axios(config_ER)
            .then(function (response) {

                response.data.map((data) => {
                    let Pid = data.pid;
                    let RoomName = 'Room ER ' + data.bed_name + '';

                    if (data.bed_name !== '00') {

                        // let sqlQuery = 'exec [dbo].[updatePidBaseMonitor] @NumberRoomName ="' + RoomName + '", @NumberPid = "' + Pid + '"';

                        // sqlRequest.query(sqlQuery, function (err, data) {
                        //     if (data) {
                        //         console.log('update => RoomName:', RoomName, 'Pid:', Pid);
                        //     } else {
                        //         console.log('not update');
                        //     }
                        // });

                        console.log(Pid);
                        console.log(RoomName);
                        // console.log(sqlQuery);
                    }
                });

            })
            .catch(function (err) {
                console.log('error :', err);
            });

    })
    .catch((err) => {
        console.log('error :', err);
    })
})