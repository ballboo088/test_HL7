const hl7 = require('simple-hl7');
const cron = require('node-cron');
const config = require('./config/test')
const PORT = config.port
const IP = config.ip


const client = hl7.Server.createTcpClient(IP, PORT);
console.log("agent start on ", IP, ':', PORT)

//create a message
const msg = new hl7.Message(
    "EPIC",
    "EPICADT",
    "SMS",
    "199912271408",
    "CHARRIS",
    ["ORU", "R01"], //This field has 2 components
    "1817457",
    "D",
    "2.5"
);

console.log('******sending message*****')
// console.log("msg =>",msg)

cron.schedule('*/5 * * * * *', function () {

    // console.log("getHL7")
    client.send(msg, (err, ack) => {
        console.log('******ack received*****')

        if (ack === undefined) {
            console.log("Cannot get HL7")

        } else {

            data = ack.log()
            hl7_message = data.toString()
            console.log("Can get HL7 \n", data)
            
            // hl7_message = hl7_message.split("")
            // hl7_message_text = hl7_message.toString().split("\r\n")
            // console.log("hl7_message_text =>",hl7_message_text)

            // for (i = 0; i < hl7_message_text.length; i++) {
            //     console.log(i,' =>',hl7_message_text[i].trim())
            // }



        }




    });

})

///////////////////CLIENT/////////////////////

