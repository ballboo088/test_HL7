const hl7 = require('simple-hl7')
const axios = require('axios')
const cron = require('node-cron');
const config = require('./config/init_config')

const PORT = config.port
const IP = config.ip
const URL = config.url

const app = hl7.tcp()
let i = 0 ; 
const TMP = {}

function getDateTime() {

    let date = new Date();

    let hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    let min = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    let sec = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    let year = date.getFullYear();

    let month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    let day = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;

}
const setDatatoDB = async (room,parameter,valuevital_sign) => {
    let BaseRoomID = '/'+room+'/'+parameter
    URL_SEND = URL +'api/NagativePressureRoom/vitalsigns'+BaseRoomID
    // console.log(URL_SEND,'===>',valuevital_sign);
    try {
        const response = await axios.put(URL_SEND, {
            value: valuevital_sign
        },
            {
                headers: {
                    "Content-Type": "application/json",
                    Authorization: '8m8SEvka7WEFaAYzvVo2WMjEgThwTE3S51NIkUC8N6GOELJbPB4DrJHh9MQWlx9AFcpvlF0D8D24BeEIymZmYa3RTxJpWM0u9RvwaYzbTFFoDvp9sKipYXASCGmH8qyTilSvWmD76OD02HCdi7Ve0wY8rxcTHfntgPV3SjZGuL5Z4egfqOfEPhLtRr48Qs3555m2uguqbheT8mZdeKSVBojtHWEfzSMe3xSP2XQFGvSwMFUpQpS176ncVHzbOwhN65C9Mj7G8cz0c7rKtYJIQcB7eBO1keEKiMIuSDh4lZb8XVRkPnAwKJWajHEjcyqwicRxcpV6ekWwqIihu4EsXB3TiCf2c8NywApywD0pgZSKpvVKLHrBKUldnZgq6c5oIDqSbJ8nvg7zPRHN8Rjar045XxVAqfIQKBaelxGXR8SOwlx5T7d9hBck7aw5YAX0WCx5J142EEYAlQefLNjIdOwZTaAIBREJvWAFOYG0gRTgiqFIlSzwmeNI9b1FGjoz'
                },
            }
        )
        console.log(BaseRoomID,'',response.data.code,": ",response.data.parameter," ",response.data.message)
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
}
console.log('******AGENT AND LISTENS ON PORT :' + IP + ':' + PORT + ' *****')
//HR, BP, O2SAT, MAP, RR, TEMP, ETCO2, ARR
let HR = ''
let O2SAT = ''
let RR = ''
let BPS = ''
let BP = ''
let MAP = ''
let ETCO2 = ''

let ROOM = ''

console.log('******message HL7 received*****')

app.use((req, res, next) => {

    if (req) {
        let date = getDateTime() ;

        message_tmp = {} ; 
        message_tmp = req.msg.log() ; 
        message_strc = req.msg ; 
        console.log("message =>", message_tmp)

        hl7_message = message_tmp.split("\n")
        // console.log(hl7_message)
            hl7_message.forEach((index) => { 
                ParameterName_tmp = ''
                if (index.split('|')[0] === 'MSH') {
                    console.log("-----------------------------------------------")
                    console.log("HEADING HL7")
                }
                else if (index.split('|')[0] === 'PID') {
                    console.log("-----------------------------------------------")
                    console.log("PATIENT")
                }
                if (index.split('|')[0] === 'PV1') {
                    // console.log("-----------------------------------------------")
                    // console.log("BED:", index.split('|')[3].split('^')[2].replace("&0&0",""))
                    console.log("-----------------------------------------------")
                    ROOM = index.split('|')[3].split('^')[2].split('&')[0]
                    console.log("ROOM|",ROOM,"|")
                }
                if (ROOM === 'Bed1') {
                    if (index.split('|')[0] === 'OBX') {
                        OBX = index.split('|')
                        OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                        OBX5_VALUE_PARAMETER = OBX[5]
                        OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                        if (index.split('|')[2] === "NM" ){
                            if (OBX3_NAME_PARAMETER === 'HR') {
                                HR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT10664001','HR', HR)
                            }
                            else if (OBX3_NAME_PARAMETER === 'RR') {
                                RR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT10664001','RR', RR)
                            }
                            else if (OBX3_NAME_PARAMETER === 'SpO2') {
                                O2SAT = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT10664001','O2SAT', O2SAT)
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'NBPs'){
                                BPS = OBX5_VALUE_PARAMETER
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'NBPd'){
                                BP = BPS +'/'+OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT10664001','BP', BP)
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'NBPm'){
                                MAP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT10664001','MAP', MAP)
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'etCO2'){
                                ETCO2 = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT10664001','ETCO2', ETCO2)
                                
                            }

                        }

                    }
                }
                if (ROOM === 'Bed2') {
                    if (index.split('|')[0] === 'OBX') {
                        OBX = index.split('|')
                        OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                        OBX5_VALUE_PARAMETER = OBX[5]
                        OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                        if (index.split('|')[2] === "NM" ){
                            if (OBX3_NAME_PARAMETER === 'HR') {
                                HR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT10664002','HR', HR)
                            }
                            else if (OBX3_NAME_PARAMETER === 'RR') {
                                RR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT10664002','RR', RR)
                            }
                            else if (OBX3_NAME_PARAMETER === 'SpO2') {
                                O2SAT = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT10664002','O2SAT', O2SAT)
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'NBPs'){
                                BPS = OBX5_VALUE_PARAMETER
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'NBPd'){
                                BP = BPS +'/'+OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT10664002','BP', BP)
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'NBPm'){
                                MAP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT10664002','MAP', MAP)
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'etCO2'){
                                ETCO2 = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT10664002','ETCO2', ETCO2)
                                
                            }

                        }

                    }
                }
                if (ROOM === 'Bed3') {
                    if (index.split('|')[0] === 'OBX') {
                        OBX = index.split('|')
                        OBX3_NAME_PARAMETER = OBX[3].split('^')[1]
                        OBX5_VALUE_PARAMETER = OBX[5]
                        OBX6_UNIT_PARAMETER = OBX[6].split('^')[1]
                        if (index.split('|')[2] === "NM" ){
                            if (OBX3_NAME_PARAMETER === 'HR') {
                                HR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT10664003','HR', HR)
                            }
                            else if (OBX3_NAME_PARAMETER === 'RR') {
                                RR = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT10664003','RR', RR)
                            }
                            else if (OBX3_NAME_PARAMETER === 'SpO2') {
                                O2SAT = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT10664003','O2SAT', O2SAT)
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'NBPs'){
                                BPS = OBX5_VALUE_PARAMETER
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'NBPd'){
                                BP = BPS +'/'+OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT10664003','BP', BP)
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'NBPm'){
                                MAP = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT10664003','MAP', MAP)
                                
                            }
                            else if (OBX3_NAME_PARAMETER === 'etCO2'){
                                ETCO2 = OBX5_VALUE_PARAMETER
                                setDatatoDB('IOT10664003','ETCO2', ETCO2)
                                
                            }

                        }

                    }
                }
            });
                
    }
    else {
        console.log("NO MESSAGE")
    }
})
app.use((err, req, res, next) => {
    
    console.log('******ERROR*****')
    console.log(err);
    
})

app.start(PORT);